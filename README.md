# trisect_i2c_sensors

[Trisect](https://rsa-perception-sensor.gitlab.io/) relies on a sub-processor to handle data acquisition and interface with embedded sensor (IMU, pressure, temperature).   This is largely because hthe AntMicro baseboard does not expose the Jetson's built-in I2C/SPI buses; but the use of a small co-processor radically simplifies the design and provides some partioning of roles.

The current base board uses an [Adafruit QtPy RP2040](https://www.adafruit.com/product/4900) which is mounted on a [custom baseboard]() which includes two leak detectors.   [BNO085 absolute oritentation sensor](https://www.adafruit.com/product/4754) and [BME280 pressure/temperature sensor](https://www.adafruit.com/product/2652) dev boards are connected over the Stemma QT I2C bus.   The QtPy runs a CircuitPython firmware which reads the sensors (leak, orientation, pressure/temperature) and publishes the data over the USB port.

The firmware reads the I2C sensors at a 1Hz and prints a set of NMEA-like strings to the ACM serial port for reading buy the Jetson.

This repo contains:

* the [firmware](firmware/) which runs on the QtPy
* the [ROS nodes](nodes/) which runs on the Trisect, receiving the data from the QtPy and re-publishing it as ROS messages.

For Further details see the [Trisect docs](https://rsa-perception-sensor.gitlab.io/trisect-docs/docs/hardware/i2c_sensors/).

# ROS node

The ROS node publishes the external sensor data ...

# CircuitPython firmware

The programmable microcontroller is an [Adafruit QtPy RP2040](https://www.adafruit.com/product/4900).  The firmware is written in [CircuitPython](https://circuitpython.org/board/adafruit_qtpy_rp2040/);  the main source file is [firmware/code.py](firmware/code.py).

The following modules from the [CircuitPython library bundle](https://circuitpython.org/libraries) must be installed in the `lib/` directory on the device:

* `adafruit_bme280/`
* `adafruit_bno08x/`

# License

This code is released under the [BSD 3-Clause License](LICENSE)
