#!/bin/bash

# Additional feature to add:
#  * autodetect the block device (using "udiskstl status"?)

# More checking could be done
if [ ! -d /media/trisect/CIRCUITPY/ ]; then
    udisksctl mount -b /dev/sda1
fi

cp firmware/code.py /media/trisect/CIRCUITPY/

udisksctl unmount -b /dev/sda1
