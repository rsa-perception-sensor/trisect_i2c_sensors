from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=["trisect_i2c_sensors"],
    scripts=["nodes/trisect_i2c_sensors"],
    # package_dir={'': 'src'}
)

setup(**d)
