# SPDX-FileCopyrightText: 2021 University of Washington
# SPDX-License-Identifier: BSD-3-Clause

import time

# import os
import board
import busio
from adafruit_bme280 import basic as adafruit_bme280
import analogio

from adafruit_bno08x.i2c import BNO08X_I2C
from adafruit_bno08x import (
    BNO_REPORT_ACCELEROMETER,
    BNO_REPORT_GYROSCOPE,
    BNO_REPORT_MAGNETOMETER,
    BNO_REPORT_ROTATION_VECTOR,
)

import bitbangio

adc = [analogio.AnalogIn(board.A2), analogio.AnalogIn(board.A3)]

# Configuration
i2c_frequency = 400000
i2c_timeout = 100000

# If set to true, use the bitbangio implementation for I2C
# If false, use the hardware I2C interface...
i2c_bitbang = False
bno_debug = False

loop_period_ns = 1e8

# Message periods given in multiples of loop_period
leak_period = 10
bme_period = 10

#

if i2c_bitbang:
    i2c = bitbangio.I2C(board.SCL1, board.SDA1, timeout=i2c_timeout)
    # frequency=i2c_frequency, timeout=i2c_timeout)
else:
    # i2c = board.I2C(frequency=i2c_frequency)
    i2c = busio.I2C(scl=board.SCL1, sda=board.SDA1, frequency=i2c_frequency)

bno = BNO08X_I2C(i2c, debug=bno_debug)
bno.enable_feature(BNO_REPORT_ACCELEROMETER)
bno.enable_feature(BNO_REPORT_GYROSCOPE)
bno.enable_feature(BNO_REPORT_MAGNETOMETER)
bno.enable_feature(BNO_REPORT_ROTATION_VECTOR)


bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)
# change this to match the location's pressure (hPa) at sea level
bme280.sea_level_pressure = 1013.25

count = 0
next_loop_time = time.monotonic_ns()

while True:
    if time.monotonic_ns() >= next_loop_time:

        count += 1
        next_loop_time += loop_period_ns

        # print("Rotation Vector Quaternion:")
        quat_i, quat_j, quat_k, quat_real = bno.quaternion
        print("$TSQUT,%f,%f,%f,%f" % (quat_i, quat_j, quat_k, quat_real))

        accel_x, accel_y, accel_z = bno.acceleration
        print("$TSACL,%f,%f,%f" % (accel_x, accel_y, accel_z))

        gyro_x, gyro_y, gyro_z = bno.gyro
        print("$TSGYR,%f,%f,%f" % (gyro_x, gyro_y, gyro_z))

        mag_x, mag_y, mag_z = bno.magnetic
        print("$TSMAG,%f,%f,%f" % (mag_x, mag_y, mag_z))

        # BME280
        if count % bme_period == 0:
            bme_temp = bme280.temperature
            bme_relhum = bme280.relative_humidity
            bme_pressure = bme280.pressure
            print("$TSBME,%f,%f,%f" % (bme_temp, bme_relhum, bme_pressure))

        # Read ADC / leak sensors
        if (count + 5) % leak_period == 0:
            adc_v = [
                channel.value / 65535 * channel.reference_voltage for channel in adc
            ]
            print("$TSLEK,%.2f,%.2f" % (adc_v[0], adc_v[1]))
